import csv
import time
start = time.time()

with open('Slack_boxes.csv') as slack_boxes_file:
    slack_boxes_file = csv.reader(slack_boxes_file)

    slack_boxes_list = []
    for row in slack_boxes_file:
        slack_boxes_list.append(row)  # slack_boxes list


with open('Big_Box_Table.csv') as big_box_file:
    big_box_file = csv.reader(big_box_file, delimiter=';')

    big_box_list = []
    for row in big_box_file:
        big_box_list.append(row)

with open('device_1.csv') as device_file:
    device_file = csv.reader(device_file, delimiter=',')

    installed_devices_list = []
    for row in device_file:
        installed_devices_list.append(row)


with open('Unique_IDs.csv') as Unique_IDs:
    Unique_IDs = csv.reader(Unique_IDs, delimiter=';')
    non_gsma_batch = []
    gsma_batch = []
    list_of_UIDs = []
    for row in Unique_IDs:
        list_of_UIDs.append(row)
        if row[3] != '8':
            gsma_batch.append(row)
        else:
            non_gsma_batch.append(row)


#functions
def get_non_gsma_boxes():
    non_gsma_big_boxes = []
    for non_gsma_device in non_gsma_batch:
        for big_box in big_box_list:
            if non_gsma_device[1] == big_box[2]:
                if big_box[1] in non_gsma_big_boxes:
                    pass
                else:
                    non_gsma_big_boxes.append(big_box[1])
            else:
                pass
    return non_gsma_big_boxes


def get_barcode_list_of_all_boxes_and_list_of_gsma_boxes_barcodes():
    list_of_all_big_box_barcodes = []
    for big_box in big_box_list:
        if big_box[1] in list_of_all_big_box_barcodes:
            pass
        else:
            list_of_all_big_box_barcodes.append(big_box[1])

    list_of_gsma_boxes_barcodes = []
    for box in list_of_all_big_box_barcodes:
        if box in non_gsma_big_boxes:
            pass
        else:
            list_of_gsma_boxes_barcodes.append(box)
    return list_of_all_big_box_barcodes,list_of_gsma_boxes_barcodes


def get_slack_boxes_list_barcodes():
    slack_boxes_list_barcode_only = []
    for slack_box in slack_boxes_list:
        if slack_box[0] in slack_boxes_list_barcode_only:
            pass
        else:
            slack_boxes_list_barcode_only.append(slack_box[0])
    return slack_boxes_list_barcode_only


def get_manufactured_boxes_not_appearing_in_slack_barcodes_only():
    manufactured_boxes_not_appearing_in_slack_barcodes = []
    for gsma_box in list_of_gsma_boxes_barcodes:
        if gsma_box in slack_boxes_list_barcode_only:
            pass
        else:
            manufactured_boxes_not_appearing_in_slack_barcodes.append(gsma_box)
    return manufactured_boxes_not_appearing_in_slack_barcodes


def get_gsma_boxes_not_appearing_in_slack_details():
    boxes_not_appearing_in_slack_details = []
    for box_not_in_slack in manufactured_boxes_not_appearing_in_slack:
        for big_box in big_box_list:
            if box_not_in_slack == big_box[1]:
                if big_box in boxes_not_appearing_in_slack_details:
                    pass
                else:
                    boxes_not_appearing_in_slack_details.append(big_box)
            else:
                pass

    return boxes_not_appearing_in_slack_details

def get_barcode_list_of_UIDs_from_device_table():
    barcode_list_of_UIDs_from_device_table = []
    for row in installed_devices_list[1:]:
        barcode = (row[1])
        length = len(barcode)
        new_barcode = (barcode[:length-1])
        if new_barcode in barcode_list_of_UIDs_from_device_table:
            pass
        else:
            barcode_list_of_UIDs_from_device_table.append(str(new_barcode))
    return barcode_list_of_UIDs_from_device_table

def get_non_slack_devices_that_were_not_installed():
    non_slack_devices_that_were_not_installed = []
    for box in big_box_list:
        if box[2] in barcode_list_of_UIDs_from_device_table:
            pass
        else:
            if box[2] in devices_from_slack:
                pass
            else:
                non_slack_devices_that_were_not_installed.append(box)
    return non_slack_devices_that_were_not_installed

#to remove slack barcodes from this list above...done!!!

def get_device_UIDs_in_a_box(box_barcode_list):
    UIDs_list = []
    for box_barcode in box_barcode_list:
        for big_box_barcode in big_box_list:
            if box_barcode == big_box_barcode[1]:
                if big_box_barcode[2] in UIDs_list:
                    pass
                else:
                    UIDs_list.append(big_box_barcode[2])
            else:
                pass
    return UIDs_list





def devices_not_installed_in_list(list_of_devices_to_check):
    list = []
    for device in list_of_devices_to_check:
        if device in barcode_list_of_UIDs_from_device_table:
            pass
        else:
            list.append(device)
    return list


def put_each_installed_device_into_its_bigbox_and_count():
   big_box_and_no_dict = {}
   for barcode in barcode_list_of_UIDs_from_device_table :
       for big_box in big_box_list:
           if barcode == big_box[2]:
               if big_box[1] in big_box_and_no_dict:
                   big_box_and_no_dict[big_box[1]]+=1
               else:
                   big_box_and_no_dict[big_box[1]]=1
           else:
               pass
   return big_box_and_no_dict

#todo get non slack devices that were not installed...done


non_gsma_big_boxes = get_non_gsma_boxes()
list_of_all_big_box_barcodes, list_of_gsma_boxes_barcodes = get_barcode_list_of_all_boxes_and_list_of_gsma_boxes_barcodes()
slack_boxes_list_barcode_only = get_slack_boxes_list_barcodes()

devices_from_slack = get_device_UIDs_in_a_box(slack_boxes_list_barcode_only)
manufactured_boxes_not_appearing_in_slack = get_manufactured_boxes_not_appearing_in_slack_barcodes_only()
Boxes_not_appearing_in_slack_details = get_gsma_boxes_not_appearing_in_slack_details()
barcode_list_of_UIDs_from_device_table = get_barcode_list_of_UIDs_from_device_table()
non_slack_devices_that_were_not_installed = get_non_slack_devices_that_were_not_installed()
devices_from_slack_not_installed = devices_not_installed_in_list(devices_from_slack)
big_box_and_no_dict = put_each_installed_device_into_its_bigbox_and_count()

print('non slack devices that were not installed', len(non_slack_devices_that_were_not_installed))
print('number of big boxes', len(big_box_and_no_dict))
print('total number of installed devices', sum(big_box_and_no_dict.values()))

#get missing big_boxes
boxes_with_no_installations_barcodes_only = []
for box in big_box_list:
    if box[1] not in big_box_and_no_dict.keys():
        if box[1] not in boxes_with_no_installations_barcodes_only:
            boxes_with_no_installations_barcodes_only.append(box[1])

#check to see if these boxes are gsma
gsma_boxes_not_installed = []
for box in boxes_with_no_installations_barcodes_only:
    if box in list_of_gsma_boxes_barcodes:
        gsma_boxes_not_installed.append(box)


print('number of gsma boxes not installed', len(gsma_boxes_not_installed))

with open('non_slack_devices_that_were_not_installed.csv','w') as f:
    for k in non_slack_devices_that_were_not_installed:
        writer = csv.writer(f)
        writer.writerow(k)

#take big box and count of non slack barcodes not installed
non_slack_big_box_and_no_dict_not_installed = {}
for box in non_slack_devices_that_were_not_installed:
    if box[1] in non_slack_big_box_and_no_dict_not_installed:
        non_slack_big_box_and_no_dict_not_installed[box[1]]+=1
    else:
        non_slack_big_box_and_no_dict_not_installed[box[1]]=1

with open('non_slack_boxes_not_installed_and_count.csv','w') as f:
    for k, v in non_slack_big_box_and_no_dict_not_installed.items():
        writer = csv.writer(f)
        writer.writerow([k,v])


slack_box_not_installed = []
slack_box_installed = []
for slack_box in slack_boxes_list_barcode_only:
    if slack_box in big_box_and_no_dict.keys():
        slack_box_installed.append(slack_box)
    else:
        slack_box_not_installed.append(slack_box)

slack_boxes_and_count = {}

for key,value in big_box_and_no_dict.items():
    if key in slack_boxes_list_barcode_only:
        slack_boxes_and_count.update({key:big_box_and_no_dict[key]})

with open('Slack_boxes_and_installation_count.csv','w') as f:
    for k, v in slack_boxes_and_count.items():
        writer = csv.writer(f)
        writer.writerow([k,v])

with open('gsma_boxes_not_installed.csv','w') as f:
    for k in gsma_boxes_not_installed:
        writer = csv.writer(f)
        writer.writerow([k])


print('slack box installed', len(slack_box_installed))
print('slack box not installed', (slack_box_not_installed))
print('devices from slack not installed',len(devices_from_slack_not_installed ) )
print('slack boxes and their count length', slack_boxes_and_count)

