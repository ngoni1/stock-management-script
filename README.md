#Python stock management script

##Current Scenario

The main purpose of the stock management script is to have all the devices that have been manufactured, installed, and in storage to be accounted for. ( i.e the sum of the devices in storage and the ones installed should simply add up to amount of devices manufactured.) At the moment we are concerned about the GSMA batch only. Thus batch 9 up to the most recent batch(batch 13).

At the moment the script takes in data from 4 csv tables. Listed below ith the data required from them.
1. Unique_Ids.csv- used to separate detectors accordingt to their different batches
2. Big_Box_Table.csv- these are the manufactured boxes with their box barcodes and the detectors put into them.
3. device_1.csv- a list of all the installed devices with their location. (This is taken from the device.csv table but only has the following coluumns to reduce reasding time (id, device_barcode, location_name, sytem_created->date)
4. Slack_boxes.csv- this a list of manufactured boxes that have been taken to communities. Ideally every device in these boxes should be installed, otherwise accounted in the slack channel(e.g ‘missing 8 detectors used for demo’, etc)

Using the tables above, useful information can be obtained and at the moment the script is producing the following csv files with useful insights on the detectors and where they are:
1. Slack_boxes_and_installation_count.csv
2. non_slack_devices_that_were_not_installed.csv
3. non_slack_boxes_not_installed_and_count.csv
4. gsma_boxes_not_installed.csv

The script also counts the total number of gsma devices manufactured and installed and the total number of all devices from slack and of the those how many have installed and those not installed.

At the moment some bit of manual sorting and rearranging of the csvs we obtain is required to produce information that is easier to analyse.

##Looking into the future

For repeated usage of the script, all the following tables above need to be up to date and in the correct format to be read by the script. The first 3 are to be downloaded from the production database and the Slack_boxes.csv file needs to be updated manually up to the latest date from the slack ‘Stock management’ channel-> Obtain the big_box_barcode, location taken to for installation and the date delivered. The script can then be run to obtain the second list csvs above.

A ‘current_stock’ table is in the making process. This gives the big_box_barcodes of the devices that are currently not installed but are being stored at their specific communities, waiting for installation.
-recording information from slack can be quite tiresome, (is there a way to automate this process?) the idea still to be implemented was to find out if its possible to access the slack API for slack to give a certain response for every message received in this channel.(e.g extract the useful information to update Slack_boxes.csv and do so automatically)





